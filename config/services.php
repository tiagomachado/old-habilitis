<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    //Socialite
    'facebook' => [
        'client_id' => '536573879715821',
        'client_secret' => 'aa71ea25628469f16eea2ce95d1639de',
        'redirect' => 'http://habilitis.dev/social/callback/facebook',
    ],

    'linkedin'=> [
        'client_id' => '774io2sxs97ush',
        'client_secret' => '5hvhlrrpS3fVPk0C',
        'redirect' => 'http://habilitis.dev/social/callback/linkedin',
    ],

];
