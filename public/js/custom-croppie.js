jQuery(function($) {

            $('#upload').on('change', function () {

                $.ajax({
                    type:"POST",
                    url:'settings/tempfile',
                    processData: false,
                    contentType: false,
                    data: new FormData($("#settings-user")[0]),
                    beforeSend: function(request) {
                        return request.setRequestHeader("X-CSRF-Token", $("meta[name='token']").attr('content'));
                    },
                    success: function(input){
                        readFile(input);
                    }

                });

                $('#upload').val('');
            });


            var  uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'circle'
                },
                boundary:{
                    width: 598,
                    height: 300,
                },
                enableZoom: false,
                showZoomer: false,
                mouseWheelZoom:false,
            });

            function readFile(input) {
                $('#myModal').modal('show');
                uploadCrop.croppie('bind',{  url:input });

            }

            $('#save-new-photo').on('click', function () {

                $.ajax({
                    type:"POST",
                    url:'settings/saveimag',
                    data:{  photo:$('img.cr-image').attr('src'),
                        points:$('#upload-demo').croppie('get')
                    },
                    beforeSend: function(request) {
                        return request.setRequestHeader("X-CSRF-Token", $("meta[name='token']").attr('content'));
                    },
                    success: function(input){
                        location.reload();
                    }
                });

            });
});

