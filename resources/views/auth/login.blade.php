@extends('layouts.auth-register')

@section('title', 'Login')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">

				@include('auth._social-icons')

				<div class="panel-body">

				{!! Form::open(array('url' => url('auth/login'), 'method' => 'post')) !!}
				<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
					<div class="controls">
						{!! Form::text('email', null, array('class' => 'form-control',
																	   'placeholder'=>'E-Mail Address')) !!}
						<span class="help-block">{{ $errors->first('email', ':message') }}</span>
					</div>
				</div>

				<div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
					<div class="controls">
						{!! Form::password('password', array('class' => 'form-control',
																		'placeholder'=>'Password')) !!}
						<span class="help-block">{{ $errors->first('password', ':message') }}</span>
					</div>
				</div>

				<div class="form-group">
						<button type="submit" class="btn btn-block btn-primary" style="margin-right: 15px;">
							Login
						</button>



				</div>

				<div class="form-group">
					<span class="checkbox login-checkbox">
							<label>
								<input type="checkbox" name="remember"> Remember Me
							</label>
					</span>
							<a  href="{{url('/auth/password') }}">Forgot Your Password?</a>
							<a class="pull-right" href='{{url('auth/register') }}'>Sign Up</a>

				</div>
				{!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
