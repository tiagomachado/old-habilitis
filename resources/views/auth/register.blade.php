@extends('layouts.auth-register')

@section('title', 'Register')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">

				@include('auth._social-icons')

				<div class="panel-body">
					<div class="container-fluid">
						<div class="row">
							{!! Form::open(array('url' => url('auth/register'), 'method' => 'post')) !!}

							<div class="form-group  {{ $errors->has('first_name') ? 'has-error' : '' }}">
								<div class="controls">
									{!! Form::text('first_name', null, array('class' => 'form-control',
																						'placeholder'=>'First Name')) !!}
									<span class="help-block">{{ $errors->first('first_name', ':message') }}</span>
								</div>
							</div>

							<div class="form-group  {{ $errors->has('last_name') ? 'has-error' : '' }}">
								<div class="controls">
									{!! Form::text('last_name', null, array('class' => 'form-control',
																						'placeholder'=>'Last Name')) !!}
									<span class="help-block">{{ $errors->first('last_name', ':message') }}</span>
								</div>
							</div>


							{{--<div class="form-group  {{ $errors->has('username') ? 'has-error' : '' }}">
								<div class="controls">
									{!! Form::text('username', null, array('class' => 'form-control')) !!}
									<span class="help-block">{{ $errors->first('username', ':message') }}</span>
								</div>
							</div>--}}


							<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
								<div class="controls">
									{!! Form::text('email', null, array('class' => 'form-control',
																				   'placeholder'=>'E-Mail Address')) !!}
									<span class="help-block">{{ $errors->first('email', ':message') }}</span>
								</div>
							</div>

							<div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
								<div class="controls">
									{!! Form::password('password', array('class' => 'form-control',
																					'placeholder'=>'Password')) !!}
									<span class="help-block">{{ $errors->first('password', ':message') }}</span>
								</div>
							</div>

							<div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
								<div class="controls">
									{!! Form::password('password_confirmation', array('class' => 'form-control',
																								 'placeholder'=>'Confirm Password')) !!}
									<span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
								</div>
							</div>

							<div class="form-group">
									<button type="submit" class="btn btn-block btn-primary">
										Register
									</button>
								<br>
								<div class="text-center">
									<a href='{{url('auth/login') }}'>Login</a>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>



				</div>
			</div>
		</div>
	</div>
</div>
@endsection
