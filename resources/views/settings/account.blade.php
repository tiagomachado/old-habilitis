@extends('layouts.mainpage')

@section('title', 'Settings / Profile')

@section('content')
    {!! flashMessageSettingsSave() !!}
    <div class="container-fluid">
        <div class="row">

            @include('settings._left-menu')


            <div class="col-md-6">
                <div class="panel panel-default form-account">

                    <div class="panel-heading">Account</div>


                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">
                                {!! Form::model($userAuth,array('id'=>'settings-user','url' => url('settings/account'),'files'=>true, 'method' => 'post')) !!}


                                <div class="form-group {{ $errors->has('photo') ? 'has-error' : '' }}">


                                    <userimg
                                            userimg="{{imgPathOnDB(Auth::user()->photo)}}"
                                            imgalt="{{authUserFullName()}}"
                                            class="img-circle settings-img-user"
                                    ></userimg>

                                    <div class="file-upload btn btn-primary">
                                        <span>Upload new picture</span>
                                        {!! Form::file('photo',array('class' => 'upload','id'=>'upload')) !!}
                                    </div>

                                        <span class="help-block">{{ $errors->first('photo', ':message') }}</span>

                                </div>

                                <div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
                                    <div class="controls">
                                        {!! Form::label('first_name', 'First Name') !!}
                                        {!! Form::text('first_name', null, array('class' => 'form-control',
                                                                                            'placeholder'=>'First Name')) !!}
                                        <span class="help-block">{{ $errors->first('first_name', ':message') }}</span>
                                    </div>
                                </div>

                                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                    <div class="controls">
                                        {!! Form::label('last_name', 'Last Name') !!}
                                        {!! Form::text('last_name', null, array('class' => 'form-control',
                                                                                            'placeholder'=>'Last Name')) !!}
                                        <span class="help-block">{{ $errors->first('last_name', ':message') }}</span>
                                    </div>
                                </div>


                                <div class="form-group {{ $errors->has('username')?'has-error':''}}">
                                    <div class="controls">
                                        {!! Form::label('username', 'Username') !!}
                                        {!! Form::text('username', null, array('class' => 'form-control',
                                                                                          'placeholder'=>'Username',
                                                                                          'v-model'=>"username",
                                                                                         )) !!}

                                        <username :username="username"></username>

                                        <span class="help-block">{{ $errors->first('username', ':message')}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn  btn-primary">
                                        Save changes
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>




                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>

                    <div id="upload-demo"></div>

                <div class="modal-footer">
                    <button id="save-new-photo" type="button" class="btn btn-primary" data-dismiss="modal">Save new profile picture</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection