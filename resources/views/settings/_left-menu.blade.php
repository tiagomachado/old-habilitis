<div class="col-md-2 col-md-offset-2">
    <div class="panel panel-default">

        <div class="panel-heading">Settings</div>
        <div class="list-group">
            <a href="{{url('/settings') }}" class="list-group-item">Details</a>
            <a href="{{url('/account') }}" class="list-group-item">Account</a>
            <a href="{{url('/password') }}" class="list-group-item">Password</a>
        </div>
    </div>
</div>