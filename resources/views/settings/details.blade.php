@extends('layouts.mainpage')

@section('title', 'Settings / Profile')

@section('content')
    {!! flashMessageSettingsSave() !!}
    <div class="container-fluid">
        <div class="row">

        @include('settings._left-menu')


            <div class="col-md-6">
                <div class="panel panel-default form-details">

                    <div class="panel-heading">Details</div>


                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">
                                {!! Form::model($userDetails,array('url' => url('settings/profile'), 'method' => 'post')) !!}

                                <div class="form-group  {{ $errors->has('age') ? 'has-error' : '' }}">
                                    <div class="controls">
                                        {!! Form::label('email', 'Age') !!}
                                        {!! Form::date('age', null, array('class' => 'form-control',
                                                                                       'placeholder'=>'Age')) !!}
                                        <span class="help-block">{{ $errors->first('age', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('location') ? 'has-error' : '' }}">
                                    <div class="controls">
                                        {!! Form::label('email', 'Location') !!}
                                        {!! Form::text('location', null, array('class' => 'form-control',
                                                                                            'placeholder'=>'Location')) !!}
                                        <span class="help-block">{{ $errors->first('location', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('bio') ? 'has-error' : '' }}">
                                    <div class="controls">
                                        {!! Form::label('email', 'Bio') !!}
                                        {!! Form::textarea('bio', null, array('class' => 'form-control',
                                                                                            'placeholder'=>'Bio')) !!}
                                        <span class="help-block">{{ $errors->first('bio', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn  btn-primary">
                                        Save changes
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>



                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
@endsection