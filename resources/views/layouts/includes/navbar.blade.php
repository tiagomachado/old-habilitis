<nav class="navbar navbar-hab navbar-fixed-top">
    <div class="container-fluid">

        <div class="navbar-buttons">
            <a href="#">
                <i class="ion-headphone"></i>
            </a>

            <a href="#">
                <i class="ion-film-marker"></i>
            </a>

            <a href="#">
                <i class="ion-images"></i>
            </a>

        </div>

        <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/') }}">
               {!!Html::image('images/nav-logo-habilitis.png',
                              'Logo',
                              array('class' => 'nav-logo')) !!}
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">

            <div class="navbar-right">
                <div class="navbar-user">
                    <a id="dLabel" data-target="#" href="{{'/settings'}}" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">

                        <userimg userimg="{{imgPathOnDB(Auth::user()->photo)}}" imgalt="{{authUserFullName()}}"></userimg>

                    </a>

                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li>
                            <a href="{{profileUrl() }}">
                                <b>{!! authUserFullName() !!}</b>
                                <p>Your profile</p>
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{url('/settings') }}">Settings</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{url('/auth/logout') }}">Logout</a>
                        </li>
                    </ul>

                </div>
            </div>




        </div><!--/.nav-collapse -->
</div>
</nav>
