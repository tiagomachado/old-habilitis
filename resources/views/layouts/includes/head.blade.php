<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="token" content="{{ Session::token() }}">
    <title>@yield('title')</title>
    {!! Html::style('http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext') !!}
    {!!  Html::style('css/app.css') !!}
    {!!  Html::style('css/croppie.css') !!}
</head>