@include('layouts.includes.head')

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">


@include('layouts.includes.navbar')

<div class="start-content">
	@yield('content')
</div>


@include('layouts.includes.footer')

</body>
</html>
