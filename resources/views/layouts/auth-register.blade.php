@include('layouts.includes.head')

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

<div class="middle">
    @yield('content')
</div>


@include('layouts.includes.footer')

</body>
</html>
