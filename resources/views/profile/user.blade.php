@extends('layouts.mainpage')

{{--@section('title', $user['firstName'].' '. $user['lastName'])--}}

@section('content')
<div class="user-profile">


    <div class="left-side">

        <userprofiledetails
                :user="{{ $user }}">
        </userprofiledetails>


    </div>

    <div class="right-side col-md-8 col-md-offset-2">
        <div class="panel panel-user">

        </div>
    </div>



</div>
@endsection