var Vue = require('vue');

Vue.use(require('vue-resource'));

import username from './components/username.vue';
import userimg from './components/userimg.vue';
import userprofiledetails from './components/userprofiledetails.vue';


/*
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#csrf-token').getAttribute('start-content');
*/


var vm = new Vue({
    el: 'body',
    data() {
        return {
            username:[],

        }
    },

    components: {
        username,
        userimg,
        userprofiledetails
    }
});
