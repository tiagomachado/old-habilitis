<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 08-02-2016
 * Time: 9:30
 */

use Carbon\Carbon;

if(!function_exists('userFullName')){

    function userFullName($model){

        return $model->first_name.' '. $model->last_name;
    }
}



if(!function_exists('authUserFullName')){

    function authUserFullName(){

        return Auth::user()->first_name.' '. Auth::user()->last_name;

    }
}




if (! function_exists('alertSettingsSave')) {

    function settingsSave(){
        if(Session::has('alertSettingsSave')) {

            $message = Session::get('settingsSave');

            return '<div class="alert alert-success fade in" >
                    <a href = "#" class="close" data-dismiss = "alert" aria-label = "close" >&times;</a >
                     '.$message.'
                    </div >';
        }
    }
}

if (! function_exists('alertSettingsSave')) {

    function alertSettingsSave($messge='Your settings have been saved'){

        return Session::flash('alertSettingsSave', $messge);

    }
}


if (! function_exists('flashMessageSettingsSave')) {

    function flashMessageSettingsSave(){
        if(Session::has('alertSettingsSave')) {

            $message = Session::get('alertSettingsSave');

            return '<div class="alert alert-success fade in" >
                    <a href = "#" class="close" data-dismiss = "alert" aria-label = "close" >&times;</a >
                     '.$message.'
                    </div >';
        }
    }
}


if (! function_exists('profileUrl')) {

    function profileUrl($idUser=null){

    if(is_null($idUser)){
        $idUser= Auth::user()->id;
    }

        $profile = \App\Models\User::find($idUser);

        $urlProfile = !empty($profile->username)?$profile->username:$profile->id;

        return url('/'.$urlProfile);


    }

}

if (! function_exists('checkAgeDate')){
    function checkAgeDate($dateAge){

        if($dateAge == '0000-00-00' ){
            return false;
        }
    $date = Carbon::parse($dateAge);
    return Carbon::createFromDate($date->year,$date->month,$date->day)->age;

    }
}