<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 13-03-2016
 * Time: 0:13
 */


if(!function_exists('randomName')){

    function randomName(){

      return  time().'_'.str_random(10).'_'.str_random(10);
    }
}


if(!function_exists('imagePath')){

    function imagePath(){

        return public_path('/images/users/'.Auth::user()->id);
    }
}

if(!function_exists('imagePathTemp')){

    function imagePathTemp(){

        return public_path('/temp');
    }
}

if(!function_exists('imageName')){

    function imageName($request,$fileType){

        return randomName().'.'.$request->file($fileType)->getClientOriginalExtension();
    }
}



if (! function_exists('userImg')) {

    function userImg($model){

        return Html::image(imgPathOnDB($model->photo),
                            userFullName());
    }

}

if (! function_exists('authUserImg')) {

    function authUserImg(){

        echo Html::image(imgPathOnDB(Auth::user()->photo),
                            authUserFullName());

    }

}


if (! function_exists('imgPathOnDB')) {

    function imgPathOnDB($imgDB){
       if(empty($imgDB)){
        return 'images/avatars/default.png';
       }

       if(substr( $imgDB, 0, 4 ) === "http"){
           return $imgDB;
       }
        return 'images/users/'.Auth::user()->id.'/'.$imgDB;
    }

}