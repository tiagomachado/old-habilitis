<?php
if(!function_exists('usenamesNotAllowed')) {

    function usenamesNotAllowed()
    {
        return [
            'setting',
            'settings',
            'account',
            'accounts',
            'password',
            'passwords',
            'auth',
            'auths'

        ];
    }
}


if(!function_exists('reservedNames')) {

    function reservedNames()
    {
        return implode(',', usenamesNotAllowed());
    }
}