<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_file';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'photo',
                            'user_id'];


    /**
     * Get the user that owns the Social.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
