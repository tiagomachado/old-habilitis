<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_social';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'provider_name',
                            'user_id_provider',
                            'user_id'];


    /**
     * Get the user that owns the Social.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
