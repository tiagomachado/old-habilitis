<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'bio',
                            'age',
                            'location'
                            ];

    /**
     * Get the user that owns the UserDetails.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
