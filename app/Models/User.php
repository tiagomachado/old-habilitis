<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'from_social',
                            'first_name',
                            'last_name',
                            'email',
                            'password',
                            'username',
                            'photo'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     *
     * Get the UserDetails record associated with the user.
     *
     */
    public function userDetails()
    {
        return $this->hasOne('App\Models\UserDetails');
    }

    /**
     *
     * Get the Social record associated with the user.
     *
     */
    public function social()
    {
        return $this->hasOne('App\Models\Social');
    }


    /**
     *
     *
     * @param $type
     * @return mixed
     */
    public function userFind($type){

        $user = $this->find($type);
        if(is_null($user)) {
            $user = $this->where('username', $type)->first();
        }

        $userDetails = $user->userDetails()->first();

        $userProfile =[
                        'name'=> $user->first_name.' '.$user->last_name,
                        'photo'=>imgPathOnDB($user->photo),
                        'bio'=>isset($userDetails->bio)?$userDetails->bio:'',
                        'age'=>isset($userDetails->age)?$userDetails->age:'',
                        'location'=>isset($userDetails->location)?$userDetails->location:''
                     ];


        return json_encode($userProfile);

    }


}
