<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class SettingsAccountPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'not_in:'.reservedNames().'|min:3|max:20|unique:users,username,'.Auth::user()->id,
            'photo'    => 'image|mimes:jpeg,png,jpg'
        ];
    }
}
