<?php

//**** LOGIN ****\\
Route::group(['middleware' => 'guest'], function () {
    //Normal Login
    // Authentication
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');

    // Registration
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

    //Social Login
    Route::get('social/{provider?}', 'Auth\SocialController@getSocialAuth');
    Route::get('social/callback/{provider?}', 'Auth\SocialController@getSocialAuthCallback');
});

//**** Authenticated User ****\\
Route::group(['middleware' => 'auth'], function () {
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    // <- Settings -> \\
    // Details Profile
    Route::get('/settings',['uses'=>'Settings\SettingsController@getDetailsProfile']);
    Route::post('/settings/profile',['uses'=>'Settings\SettingsController@postDetailsProfile']);
    // Account Profile
    Route::get('/account',['uses'=>'Settings\SettingsController@getAccountProfile']);
    Route::post('/settings/account',['uses'=>'Settings\SettingsController@postAccountProfile']);
    //Temp File
    Route::post('/settings/saveimag',['uses'=>'Settings\SettingsController@postSaveUserImag']);
    Route::post('/settings/tempfile',['uses'=>'Settings\SettingsController@postTempFile']);

    // <- Feeds -> \\
    Route::get('/',['uses'=>'Feeds\FeedsController@index']);
    // <- Profile -> \\
    Route::get('/{id}',['uses'=>'Profile\ProfileController@index']);

});