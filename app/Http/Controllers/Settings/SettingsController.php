<?php

namespace App\Http\Controllers\Settings;

use App\Http\Requests\SettingsAccountPostRequest;
use App\Http\Requests\SettingsDetailsPostRequest;
use App\Models\TempFile;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{




    public function getDetailsProfile()
    {
        $userDetails = UserDetails::where('user_id', '=', Auth::user()->id)->first();

        return view('settings.details')
                ->with('userDetails',$userDetails);
    }


    public function postDetailsProfile(SettingsDetailsPostRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $input = $request->all();
        $userDetails = $user->UserDetails ?: new UserDetails();
        $userDetails->bio=$input['bio'];
        $userDetails->age=$input['age'];
        $userDetails->location=$input['location'];
        $user->userDetails()->save($userDetails);


        alertSettingsSave();

        return back();
    }



    public function getAccountProfile()
    {

        return view('settings.account')
            ->with('userAuth',Auth::user());
    }


    public function postAccountProfile(SettingsAccountPostRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $input = $request->all();
        $user->first_name = $input['first_name'];
        $user->last_name  = $input['last_name'];
        $user->username   = $input['username'];
        $user->save();

        alertSettingsSave();

        return back();
    }



    public function postTempFile(Request $request)
    {
        $imageName = imageName($request,'photo');

        $tempFile = new TempFile();
        $tempFile->photo = $imageName;
        $tempFile->user_id = Auth::user()->id;
        $tempFile->save();


        $request->file('photo')->move(imagePathTemp(),$imageName);

        return 'temp/'.$imageName;

    }


    public function postSaveUserImag(Request $request)
    {
        $saveImg = $request->all();

        $points = $saveImg['points']['points'];
        $photo  = $saveImg['photo'];

        $img = Image::make($photo);
        $img->crop(200,200,$points[0],$points[1]);
        $imageName= randomName().'.'.$img->extension;

        $img->save(imagePath().'/'.$imageName);


        $user = User::find(Auth::user()->id);
        $user->photo  = $imageName;
        $user->save();



    }



}
