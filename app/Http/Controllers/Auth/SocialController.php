<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\Social;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{

    public function __construct(){
        $this->middleware('guest');
    }

    public function getSocialAuth($provider=null)
    {
        if(!config("services.$provider"))
        {
            abort('404');
        }

        return Socialite::driver($provider)->redirect();

    }


    public function getSocialAuthCallback($provider=null)
    {

       if($user = Socialite::driver($provider)->user()){
           /*dd($user);*/

           Auth::login($this->createUserfromSocial($user,$provider));

           return redirect('/');


        }else{
            return 'Something went wrong !!!';
        }
    }


    private function createUserfromSocial($user,$provider){
        $userExist = User::where('email', '=', $user->email)->first();
        if(!$userExist){
            $name = explode(" ",$user->name);

            $newUser = new User;
            $newUser->first_name = $name[0];
            $newUser->last_name = $name[1];
            $newUser->email = $user->email;
            $newUser->photo=$user->avatar;
            $newUser->from_social=true;
                if($newUser->save()){
                    $userSocial = new Social;
                    $userSocial->provider_name = $provider;
                    $userSocial->user_id_provider = $user->id;
                    $userSocial->user_id =$newUser->id;
                    $userSocial->save();
                }
            return $newUser;
        }else{
           return $userExist;
        }
    }

}
