var elixir  =   require('laravel-elixir');
                require('laravel-elixir-scss-lint');
                require('elixir-jshint');
                require('laravel-elixir-vueify');

elixir(function(mix) {
     mix.scssLint('resources/assets/sass/**/*.scss')
        .sass('app.scss')
        .browserify('main.js')
        .copy('resources/assets/js/custom/custom-croppie.js', 'public/js/custom-croppie.js')
});
